define(function() {

    angular
        .module('coreModule')
        .registerController('homeController', ['$scope', function($scope) {
            console.info('coreModule.homeController()...');   

            $scope.title = 'Hello from Home';
        }]);

});