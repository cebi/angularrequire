define(function() {

    angular
        .module('coreModule')
        .registerController('aboutController', ['$scope', function($scope) {
            console.info('coreModule.aboutController()...');
            
            $scope.title = 'Hello from About';
        }]);

});