define(['modules/core/runners/logRunner'], function(logRunner) {

    var coreModule = angular.module('coreModule', ['ngRoute', 'contactModule']);
    
    coreModule.run(logRunner);

    coreModule.config(['$controllerProvider', function($controllerProvider) {
        coreModule.registerController = $controllerProvider.register;
    }]);

    coreModule.config(['$routeProvider', function($routeProvider) {

        var homeResolver = function(controllerPath)
        {
            return {
                load: ['$q', function($q) {
                    var defered = $q.defer();
                    require([controllerPath], function() {
                        defered.resolve();
                    });    
                    return defered.promise;
                }]
            };
        };

        $routeProvider
            .when('/', { 
                controller : 'homeController', 
                templateUrl: '/app/modules/core/views/home.html',
                resolve : homeResolver('modules/core/controllers/homeController')
            })        
            .when('/home', { 
                controller : 'homeController', 
                templateUrl: '/app/modules/core/views/home.html',
                resolve: homeResolver('modules/core/controllers/homeController')
            })
            .when('/about', { 
                controller : 'aboutController', 
                templateUrl: '/app/modules/core/views/about.html',
                resolve: homeResolver('modules/core/controllers/aboutController')
            });
    }]);

    require(['modules/core/controllerReferences'], function(references) {
        require(references, function() {
            angular.bootstrap(document, ['coreModule']);    
        });
    });

});