define(function() {

    var contactModule = angular.module('contactModule', ['ngRoute']);

    contactModule.config(['$controllerProvider', function($controllerProvider) {
        console.info('contactModule.config()...');
        contactModule.registerController = $controllerProvider.register;        
    }]);

    contactModule.config(['$routeProvider', function($routeProvider) {
        console.info('contactModule.config()...');

        var controllerResolver = function(controllerPath)
        {
            console.info('contactModule.config().controllerResolver()...');
            return {
                load: ['$q', function($q) {
                    var defered = $q.defer();
                    require([controllerPath], function() {
                        defered.resolve();
                    });    
                    return defered.promise;
                }]
            };
        };

        $routeProvider       
            .when('/contact', { 
                controller : 'listController', 
                templateUrl: '/app/modules/contact/views/list.html',
                resolve: controllerResolver('modules/contact/controllers/listController')
            })
            .when('/contact/details', { 
                controller : 'detailsController', 
                templateUrl: '/app/modules/contact/views/details.html',
                resolve: controllerResolver('modules/contact/controllers/detailsController')
            });            
    }]);

    contactModule.run(function() {
        console.info('contactModule.run()...');
    });

});