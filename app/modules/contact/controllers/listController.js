define(function() {

    angular
        .module('contactModule')
        .registerController('listController', ['$scope', function($scope) {
            console.info('contactModule.listController()...');
            
            $scope.title = 'Hello from contact.listController';
        }]);

});