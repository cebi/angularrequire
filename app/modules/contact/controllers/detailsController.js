define(function() {

    angular
        .module('contactModule')
        .registerController('detailsController', ['$scope', function($scope) {
            console.info('contactModule.detailsController()...');
            
            $scope.title = 'Hello from contact.detailsController';
        }]);

});