require.config({

    paths: {
        'angular' : '../node_modules/angular/angular.min',
        'angular-route' : '../node_modules/angular-route/angular-route.min',
        'jquery' : '../node_modules/jquery/dist/jquery.min',
        'bootstrap' : '../node_modules/bootstrap/dist/js/bootstrap.min',

        // load the modules
        'coreModule' : '../app/modules/core/coreModule',
        'contactModule' : '../app/modules/contact/contactModule'
    },

    shim: {
        // external libs dependency
        'angular' : { deps : ['jquery'] },
        'bootstrap' : { deps : ['jquery'] },
        'angular-route' : { deps : ['angular'] },

        // internal dependency
        'coreModule' : { deps: ['angular-route', 'contactModule'] },
        'contactModule' : { deps : ['angular-route'] },
    }

});

require(['coreModule'], function() {
    console.info('MAIN: everything is loaded..');
});